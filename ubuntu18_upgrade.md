Upgrade from Ubuntu 16 to Ubuntu 18
===================================

If you have a running NRP installation in Ubuntu 16 and would like to upgrade to Ubuntu 18 to get NRP updates, follow the following steps.

1. In your Ubuntu 16, uninstall ROS

        sudo apt-get remove ros-kinetic-*
        sudo apt autoremove

2. Upgrade your Ubuntu with the regular Ubuntu updating tool

3. In Ubuntu 18, install ROS and dependencies by going through **steps 1.3, 2, 3.1 to 3.5 and 8** from the [standard source installation guide](https://bitbucket.org/hbpneurorobotics/neurorobotics-platform/src/master/README.md).

4. Delete your NRP virtualenvs

        cd ~/.opt
        rm -rf platform_venv
        cd $HBP/nrpBackendProxy
        rm -rf proxy_virtualenv

5. Update the NRP

        cd $HBP/user-scripts
        git pull --rebase
        ./update_nrp update all

    When the build script detects build failures, it will ask you to remove build directories. Do so manually and restart the "update_nrp update all" command every time this happens. Eventually, you should have a working NRP.

6. Update broken experiments. If some of your experiments that you cloned from templates don't behave properly, consider re-cloning them and port your changes. OpenSim experiments (with muscles) might be affected.